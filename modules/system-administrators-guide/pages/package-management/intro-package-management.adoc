include::{partialsdir}/entities.adoc[]

= Package Management

There are two types of {MAJOROS} system; "`traditional`", which use DNF, and "`image-based`" which use rpm-ostree.

== Traditional package management

On a traditional {MAJOROS} system, software is divided into RPM packages, which
can be managed via [application]*DNF*.

== Hybrid image/package management

The "`Atomic`" variant of {MAJOROS} uses [application]*rpm-ostree* to update the
host. `rpm-ostree` is a hybrid image/package system. By default, installations
are in "`image`" mode using OSTree, but additional packages can be installed.  It
also supports overrides, rebases, and many other features.  For more information,
see link:++https://www.projectatomic.io/docs/os-updates/++[its online documentation].

Additionally, the `atomic` CLI supports installation of system containers, which
are Docker/OCI images distinct from the host user space.  For more information,
see link:++https://www.projectatomic.io/docs/usr-bin-atomic/++[its online documentation].
