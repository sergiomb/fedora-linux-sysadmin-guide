
:experimental:
include::{partialsdir}/entities.adoc[]

[[chap-Gaining_Privileges]]
= Gaining Privileges

System administrators, and in some cases users, need to perform certain tasks with administrative access. Accessing the system as the `root` user is potentially dangerous and can lead to widespread damage to the system and data. This chapter covers ways to gain administrative privileges using setuid programs such as [command]#su# and [command]#sudo#. These programs allow specific users to perform tasks which would normally be available only to the `root` user while maintaining a higher level of control and system security.

See the link:++https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Security_Guide/++[Red{nbsp}Hat Enterprise{nbsp}Linux{nbsp}7 Security Guide] for more information on administrative controls, potential dangers, and ways to prevent data loss resulting from improper use of privileged access.

[[sect-Gaining_Privileges-The_su_Command]]
== The su Command

When a user executes the [command]#su# command, they are prompted for the `root` password and, after authentication, are given a `root` shell prompt.

Once logged in using the [command]#su# command, the user *is* the `root` user and has absolute administrative access to the system. Note that this access is still subject to the restrictions imposed by SELinux, if it is enabled. In addition, once a user has become `root`, it is possible for them to use the [command]#su# command to change to any other user on the system without being prompted for a password.

Because this program is so powerful, administrators within an organization may want to limit who has access to the command.

One of the simplest ways to do this is to add users to the special administrative group called _wheel_. To do this, type the following command as `root`:

[subs="macros"]
----
~]# usermod -a -G wheel pass:quotes[_username_]
----

In the previous command, replace _username_ with the user name you want to add to the `wheel` group.

You can also use the [application]*Users* settings tool to modify group memberships, as follows. Note that you need administrator privileges to perform this procedure.

. Press the kbd:[Super] key to enter the Activities Overview, type [command]#Users# and then press kbd:[Enter]. The [application]*Users* settings tool appears. The kbd:[Super] key appears in a variety of guises, depending on the keyboard and other hardware, but often as either the Windows or Command key, and typically to the left of the kbd:[Spacebar].

. To enable making changes, click the btn:[Unlock] button, and enter a valid administrator password.

. Click a user icon in the left column to display the user's properties in the right-hand pane.

. Change the Account Type from `Standard` to `Administrator`. This will add the user to the `wheel` group.

See xref:basic-system-configuration/Managing_Users_and_Groups.adoc#s1-users-configui[Managing Users in a Graphical Environment] for more information about the [application]*Users* tool.

After you add the desired users to the `wheel` group, it is advisable to only allow these specific users to use the [command]#su# command. To do this, edit the PAM configuration file for [command]#su#, `/etc/pam.d/su`. Open this file in a text editor and uncomment the following line by removing the `#` character:

----
#auth           required        pam_wheel.so use_uid
----

This change means that only members of the administrative group `wheel` can switch to another user using the [command]#su# command.

.Note
[NOTE]
====

The `root` user is part of the `wheel` group by default.

====

[[sect-Gaining_Privileges-The_sudo_Command]]
== The sudo Command

The [command]#sudo# command offers another approach to giving users administrative access. When trusted users precede an administrative command with [command]#sudo#, they are prompted for *their own* password. Then, when they have been authenticated and assuming that the command is permitted, the administrative command is executed as if they were the `root` user.

The basic format of the [command]#sudo# command is as follows:

[subs="quotes, macros"]
----
[command]#sudo# _command_
----

In the above example, _command_ would be replaced by a command normally reserved for the `root` user, such as [command]#mount#.

The [command]#sudo# command allows for a high degree of flexibility. For instance, only users listed in the `/etc/sudoers` configuration file are allowed to use the [command]#sudo# command and the command is executed in *the user's* shell, not a `root` shell. This means the `root` shell can be completely disabled as shown in the link:++https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Security_Guide/++[Red{nbsp}Hat Enterprise{nbsp}Linux{nbsp}7 Security Guide].

Each successful authentication using the [command]#sudo# command is logged to the file `/var/log/messages` and the command issued along with the issuer's user name is logged to the file `/var/log/secure`. If additional logging is required, use the `pam_tty_audit` module to enable TTY auditing for specified users by adding the following line to your `/etc/pam.d/system-auth` file:

[subs="macros"]
----
session required pam_tty_audit.so disable=pass:quotes[_pattern_] enable=pass:quotes[_pattern_]

----

where _pattern_ represents a comma-separated listing of users with an optional use of globs. For example, the following configuration will enable TTY auditing for the `root` user and disable it for all other users:

----
session required pam_tty_audit.so disable=* enable=root
----

Another advantage of the [command]#sudo# command is that an administrator can allow different users access to specific commands based on their needs.

Administrators wanting to edit the [command]#sudo# configuration file, `/etc/sudoers`, should use the [command]#visudo# command.

To give someone full administrative privileges, type [command]#visudo# and add a line similar to the following in the user privilege specification section:

[subs="quotes"]
----
juan ALL=(ALL) ALL
----

This example states that the user, `juan`, can use [command]#sudo# from any host and execute any command.

The example below illustrates the granularity possible when configuring [command]#sudo#:

[subs="quotes"]
----
%users localhost=/sbin/shutdown -h now
----

This example states that any member of the `users` system group can issue the command [command]#/sbin/shutdown -h now# as long as it is issued from the console.

The man page for `sudoers` has a detailed listing of options for this file.

.Important
[IMPORTANT]
====

There are several potential risks to keep in mind when using the [command]#sudo# command. You can avoid them by editing the `/etc/sudoers` configuration file using [command]#visudo# as described above. Leaving the `/etc/sudoers` file in its default state gives every user in the `wheel` group unlimited `root` access.

* By default, [command]#sudo# stores the sudoer's password for a five minute timeout period. Any subsequent uses of the command during this period will not prompt the user for a password. This could be exploited by an attacker if the user leaves their workstation unattended and unlocked while still being logged in. This behavior can be changed by adding the following line to the `/etc/sudoers` file:
+
[subs="macros"]
----
Defaults    timestamp_timeout=pass:quotes[_value_]
----
+
where _value_ is the desired timeout length in minutes. Setting the _value_ to 0 causes [command]#sudo# to require a password every time.

* If a sudoer's account is compromised, an attacker can use [command]#sudo# to open a new shell with administrative privileges:
+
[subs="quotes, macros"]
----
[command]#sudo /bin/bash#
----
+
Opening a new shell as `root` in this or similar fashion gives the attacker administrative access for a theoretically unlimited amount of time, bypassing the timeout period specified in the `/etc/sudoers` file and never requiring the attacker to input a password for [command]#sudo# again until the newly opened session is closed.

====

[[sect-Gaining_Privileges-Additional_Resources]]
== Additional Resources

While programs allowing users to gain administrative privileges are a potential security risk, security itself is beyond the scope of this particular book. You should therefore refer to the resources listed below for more information regarding security and privileged access.

.Installed Documentation

* `su`(1) — The manual page for [command]#su# provides information regarding the options available with this command.

* `sudo`(8) — The manual page for [command]#sudo# includes a detailed description of this command and lists options available for customizing its behavior.

* `pam`(8) — The manual page describing the use of Pluggable Authentication Modules (PAM) for Linux.

.Online Documentation

* The link:++https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Security_Guide/++[Red{nbsp}Hat Enterprise{nbsp}Linux{nbsp}7 Security Guide] provides a more in-depth look at potential security issues pertaining to setuid programs as well as techniques used to alleviate these risks.

.See Also

* xref:basic-system-configuration/Managing_Users_and_Groups.adoc#ch-Managing_Users_and_Groups[Managing Users and Groups] documents how to manage system users and groups in the graphical user interface and on the command line.
